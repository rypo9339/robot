﻿using System.Reflection;

[assembly: AssemblyCompany("Dimension Engineering LLC")]
[assembly: AssemblyCopyright("Copyright © 2012 Dimension Engineering LLC <http://www.dimensionengineering.com>")]
[assembly: AssemblyProduct("C# Library for SyRen/Sabertooth")]
[assembly: AssemblyTitle("PacketSerialExample.DiffDriveSweep")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
