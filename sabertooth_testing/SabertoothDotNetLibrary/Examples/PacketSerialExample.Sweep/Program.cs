﻿#region License
/*
SyRen/Sabertooth Library for C# and .NET
Copyright (c) 2012 Dimension Engineering LLC
http://www.dimensionengineering.com/dotnet

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE
USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#endregion

using System;
using System.Threading;
using DimensionEngineering;

namespace PacketSerialExample.Sweep
{
    public class Program
    {
        public static void Main()
        {
            // The Sabertooth is on address 128. We'll name its object ST.
            // If you've set up your Sabertooth on a different address, of course change
            // that here. For how to configure address, etc. see the DIP Switch Wizard for
            //   Sabertooth - http://www.dimensionengineering.com/datasheets/SabertoothDIPWizard/start.htm
            //   SyRen      - http://www.dimensionengineering.com/datasheets/SyrenDIPWizard/start.htm
            // Be sure to select Packetized Serial Mode for use with this library.
            //
            // On that note, you can use this library for SyRen just as easily.
            // The diff-drive commands (drive, turn) do not work on a SyRen, of course, but it will respond correctly
            // if you command motor 1 to do something (ST.Motor(1, ...)), just like a Sabertooth.
            // This sample uses a baud rate of 9600.
            //
            // Replace COM2 with the port you use.
            //
            // For a USB-to-TTL serial converter from a PC, the connections to make are:
            //   TX  (Transmit) -> Sabertooth S1
            //   GND (Ground)   -> Sabertooth 0V
            //
            // For Netduino, the connections to make are:
            //   Netduino D3  -> Sabertooth S1
            //   Netduino GND -> Sabertooth 0V
            //   Netduino 5V  -> Sabertooth 5V (OPTIONAL, if you want the Sabertooth to power the Netduino)
            Sabertooth ST = new Sabertooth("COM2", 9600);

            // Send the autobaud command to the Sabertooth controller(s).
            // NOTE: *Not all* Sabertooth controllers need this command.
            //       It doesn't hurt anything, but V2 controllers use an
            //       EEPROM setting (changeable with the function SetBaudRate) to set
            //       the baud rate instead of detecting with autobaud.
            //
            //       If you have a 2x12, 2x25 V2, 2x60 or SyRen 50, you can remove
            //       the autobaud line and save yourself two seconds of startup delay.
            ST.AutoBaud();

            while (true)
            {
                int power;

                // Ramp motor 1 from -127 to 127 (full reverse to full forward),
                // waiting 20 ms (1/50th of a second) per value.
                for (power = -127; power <= 127; power++)
                {
                    ST.Motor(1, power);
                    Thread.Sleep(20);
                }

                // Now go back the way we came.
                for (power = 127; power >= -127; power--)
                {
                    ST.Motor(1, power); // Tip for SyRen users:
                    Thread.Sleep(20);   //   Typing ST.Motor(power) does the same thing as ST.Motor(1, power).
                                        //   Since SyRen doesn't have a motor 2, this alternative can save you typing.
                }
            }
        }
    }
}
