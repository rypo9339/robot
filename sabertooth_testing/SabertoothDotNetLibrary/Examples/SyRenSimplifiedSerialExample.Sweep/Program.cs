﻿#region License
/*
SyRen/Sabertooth Library for C# and .NET
Copyright (c) 2012 Dimension Engineering LLC
http://www.dimensionengineering.com/dotnet

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE
USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#endregion

using System;
using System.Threading;
using DimensionEngineering;

namespace SyRenSimplifiedSerialExample.Sweep
{
    public class Program
    {
        public static void Main()
        {
            // We'll name the SyRen object SR.
            // For how to configure the SyRen, see the DIP Switch Wizard for
            //   http://www.dimensionengineering.com/datasheets/SyrenDIPWizard/start.htm
            // Be sure to select Simplified Serial Mode for use with this library.
            // This sample uses a baud rate of 9600.
            //
            // Replace COM2 with the port you use.
            //
            // For a USB-to-TTL serial converter from a PC, the connections to make are:
            //   TX  (Transmit) -> Sabertooth S1
            //   GND (Ground)   -> Sabertooth 0V
            //
            // For Netduino, the connections to make are:
            //   Netduino D3  -> SyRen S1
            //   Netduino GND -> SyRen 0V
            //   Netduino 5V  -> SyRen 5V (OPTIONAL, if you want the Sabertooth to power the Netduino)
            SyRenSimplified SR = new SyRenSimplified("COM2", 9600);

            while (true)
            {
                int power;

                // Ramp the motor from -127 to 127 (full reverse to full forward),
                // waiting 20 ms (1/50th of a second) per value.
                for (power = -127; power <= 127; power++)
                {
                    SR.Motor(power);
                    Thread.Sleep(20);
                }

                // Now go back the way we came.
                for (power = 127; power >= -127; power--)
                {
                    SR.Motor(power);
                    Thread.Sleep(20);
                }
            }
        }
    }
}
