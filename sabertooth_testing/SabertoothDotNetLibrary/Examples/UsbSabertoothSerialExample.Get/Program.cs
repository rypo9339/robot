﻿#region License
/*
SyRen/Sabertooth Library for C# and .NET
Copyright (c) 2013-2014 Dimension Engineering LLC
http://www.dimensionengineering.com/dotnet

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE
USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#endregion

using System;
using System.Threading;
using DimensionEngineering;

// This example is essentially the Sweep sample, but adapted to also show
// queries of battery voltage, current, and temperature.
// NOTE: Current readings on the Sabertooth 2x32 are inaccurate below about 5-10A.

namespace UsbSabertoothSerialExample.Get
{
    public class Program
    {
        public static void Main()
        {
            // We'll name the Sabertooth object ST, and the serial port object Connection.
            // For how to configure the Sabertooth, see the DIP Switch Wizard at
            //   http://www.dimensionengineering.com/datasheets/USBSabertoothDIPWizard/start.htm
            //
            // The UsbSabertooth class exposes features that only exist on USB-enabled Sabertooth motor drivers, such as
            // 12-bit motor outputs, power outputs, control over freewheeling, motor current read-back, and User Mode variables.
            // If you do not need these features, and want your code to be compatible with all Sabertooth/SyRen motor drivers,
            // including those that are not USB-enabled, use the Sabertooth class instead.
            //
            // Replace COM2 with the name of the Sabertooth's serial port. 
            // If you do not know, check Device Manager. It is located in Control Panel -> Administrative Tools.
            // Make sure the Sabertooth's USB serial drivers are installed. They are included in DEScribe.
            //
            // If you are controlling the USB-enabled Sabertooth over USB, be sure to select USB Mode in the DIP Switch Wizard.
            //
            // If you are instead controlling the USB-enabled Sabertooth with a USB-to-TTL serial converter,
            // the connections to make are:
            //   TX  (Transmit) -> Sabertooth S1
            //   RX  (Receive)  -> Sabertooth S2
            //   GND (Ground)   -> Sabertooth 0V
            // Be sure to select Packet Serial Mode in the DIP Switch Wizard.
            UsbSabertoothSerial Connection = new UsbSabertoothSerial("COM200", 9600);
            UsbSabertooth ST = new UsbSabertooth(Connection);

            // Ramp motor 1 from 0 to 2047 (stop to full forward),
            // waiting 20 ms (1/50th of a second) per value. Query along the way.
            Console.WriteLine("Ramping stop to full forward...");

            for (int power = 0; power <= 2047; power += 16)
            {
                ST.Motor(1, power);
                Console.WriteLine("Battery: {0}, Current: {1}, Temperature: {2}", ST.GetBattery(1), ST.GetCurrent(1), ST.GetTemperature(1));
                Thread.Sleep(20);
            }

            // Ramp motor 1 from -2047 to 2047 (full forward to full reverse),
            // waiting 20 ms (1/50th of a second) per value. Query along the way.
            Console.WriteLine("Ramping full forward to full reverse...");

            for (int power = 2047; power >= -2047; power -= 16)
            {
                ST.Motor(1, power);
                Console.WriteLine("Battery: {0}, Current: {1}, Temperature: {2}", ST.GetBattery(1), ST.GetCurrent(1), ST.GetTemperature(1));
                Thread.Sleep(20);
            }

            // Now go back to stop. More queries...
            Console.WriteLine("Ramping full reverse to stop...");

            for (int power = -2047; power <= 0; power += 16)
            {
                ST.Motor(1, power);
                Console.WriteLine("Battery: {0}, Current: {1}, Temperature: {2}", ST.GetBattery(1), ST.GetCurrent(1), ST.GetTemperature(1));
                Thread.Sleep(20);
            }

            Connection.Close();
        }
    }
}
