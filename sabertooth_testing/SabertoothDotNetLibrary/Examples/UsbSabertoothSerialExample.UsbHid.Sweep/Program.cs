﻿#region License
/*
SyRen/Sabertooth Library for C# and .NET
Copyright (c) 2013-2014 Dimension Engineering LLC
http://www.dimensionengineering.com/dotnet

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE
USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#endregion

using System;
using System.Threading;
using DimensionEngineering;
using HidSharp;

namespace UsbSabertoothSerialExample.UsbHid.Sweep
{
    public class Program
    {
        public static void Main()
        {
            // This example demonstrates communicating over USB HID. Setting up USB HID is slightly more complicated to set up than
            // the normal serial approach but does not require any drivers to be installed. Once it's set up, you can use the
            // Sabertooth in the normal way. You will need the HidSharp library.
            //
            // For how to configure the Sabertooth, see the DIP Switch Wizard at
            //   http://www.dimensionengineering.com/datasheets/USBSabertoothDIPWizard/start.htm
            // Be sure to select USB Mode for use with this example.
            //
            // The UsbSabertooth class exposes features that only exist on USB-enabled Sabertooth motor drivers, such as
            // 12-bit motor outputs, power outputs, control over freewheeling, motor current read-back, and User Mode variables.
            // If you do not need these features, and want your code to be compatible with all Sabertooth/SyRen motor drivers,
            // including those that are not USB-enabled, use the Sabertooth class instead.

            // First, find the USB Sabertooth.
            // Dimension Engineering's USB Vendor ID is 9867 and Sabertooth 2x32's USB Product ID is 513.
            HidDeviceLoader HidDeviceLoader = new HidDeviceLoader();
            HidDevice HidDevice = HidDeviceLoader.GetDeviceOrDefault(9867, 513);
            if (HidDevice == null)
            {
                Console.WriteLine("No Sabertooth 2x32 was detected.");
                return;
            }

            Console.WriteLine("Found a Sabertooth 2x32 with serial number {0}.", HidDevice.SerialNumber);

            // Now that we've found the Sabertooth, open a raw USB HID stream.
            HidStream HidStream;
            if (!HidDevice.TryOpen(out HidStream))
            {
                Console.WriteLine("Unable to open the Sabertooth 2x32. Another program may be using it.");
                return;
            }

            // Create the connection from the raw USB HID stream.
            UsbSabertooth ST = UsbSabertooth.FromHidStream(HidStream);

            // Ramp motor 1 from 0 to 2047 (stop to full forward),
            // waiting 20 ms (1/50th of a second) per value.
            Console.WriteLine("Ramping stop to full forward...");

            for (int power = 0; power <= 2047; power += 16)
            {
                ST.Motor(1, power);
                Thread.Sleep(20);
            }

            // Ramp motor 1 from -2047 to 2047 (full forward to full reverse),
            // waiting 20 ms (1/50th of a second) per value.
            Console.WriteLine("Ramping full forward to full reverse...");

            for (int power = 2047; power >= -2047; power -= 16)
            {
                ST.Motor(1, power);
                Thread.Sleep(20);
            }

            // Now go back to stop.
            Console.WriteLine("Ramping full reverse to stop...");

            for (int power = -2047; power <= 0; power += 16)
            {
                ST.Motor(1, power); // Tip:
                Thread.Sleep(20);   //   Typing ST.Motor(power) does the same thing as ST.Motor(1, power).
                                    //   If you often use only one motor, this alternative can save you typing.
            }

            ST.Connection.Close();
        }
    }
}
