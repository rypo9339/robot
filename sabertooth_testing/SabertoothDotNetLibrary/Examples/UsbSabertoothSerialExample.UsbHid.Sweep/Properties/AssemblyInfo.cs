﻿using System.Reflection;

[assembly: AssemblyCompany("Dimension Engineering LLC")]
[assembly: AssemblyCopyright("Copyright © 2014 Dimension Engineering LLC <http://www.dimensionengineering.com>")]
[assembly: AssemblyProduct("SyRen/Sabertooth Library for C# and .NET")]
[assembly: AssemblyTitle("UsbSabertoothSerialExample.UsbHid.Sweep")]
[assembly: AssemblyVersion("1.6.0.0")]
[assembly: AssemblyFileVersion("1.6.0.0")]
