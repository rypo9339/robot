﻿using System.Reflection;

[assembly: AssemblyCompany("Dimension Engineering LLC")]
[assembly: AssemblyCopyright("Copyright © 2012 Dimension Engineering LLC <http://www.dimensionengineering.com>")]
[assembly: AssemblyProduct("SyRen/Sabertooth Library for C# and .NET")]
[assembly: AssemblyTitle("SabertoothSimplifiedSerialExample.Sweep")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
