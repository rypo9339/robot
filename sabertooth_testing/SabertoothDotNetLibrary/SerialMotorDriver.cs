#region License
/*
SyRen/Sabertooth Library for C# and .NET
Copyright (c) 2012-2013 Dimension Engineering LLC
http://www.dimensionengineering.com/dotnet

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE
USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#endregion

using System;
using System.IO;
using System.IO.Ports;
using System.Runtime.InteropServices;

namespace DimensionEngineering
{
    /// <summary>
    /// Supports the serial motor driver classes.
    /// </summary>
#if !NETMF
    [ClassInterface(ClassInterfaceType.AutoDispatch)]
    [ComVisible(true)]
    [Guid("65536A59-BF1E-425D-A434-570C16AEA247")]
    [ProgId("DE.SerialMotorDriver")]
#endif
    public abstract class SerialMotorDriver : IDisposable
    {
        bool _open;
        Stream _port;
        bool _portOwned;

        /// <summary>
        /// Initializes a new instance of the <see cref="SerialMotorDriver"/> class.
        /// </summary>
        protected SerialMotorDriver()
        {

        }

        /// <summary>
        /// Closes the serial port, if the instance has ownership of it.
        /// </summary>
        public void Close()
        {
            if (_port != null && _portOwned)
            {
                _port.Close();
            }

            OnClosed();
            _open = false;
            _port = null;
        }

        void IDisposable.Dispose()
        {
            Close();
        }

        internal void RequirePort()
        {
            if (Port == null) { throw new InvalidOperationException("No port is set."); }
        }

        /// <summary>
        /// Opens the specified serial port.
        /// </summary>
        /// <param name="portName">The port to open.</param>
        /// <param name="baudRate">The baud rate to use.</param>
        public void Open(string portName, int baudRate)
        {
            if (portName == null) { throw new ArgumentNullException("portName"); }

            SerialPort port = null;
            try
            {
                port = new SerialPort(portName, baudRate);
                port.Open(); Open(port, true);
            }
            catch
            {
                if (port != null) { port.Close(); }
                throw;
            }
        }

        /// <summary>
        /// Uses an already-opened serial port.
        /// </summary>
        /// <param name="port">The port to use.</param>
        [ComVisible(false)]
        public void Open(SerialPort port)
        {
            Open(port, false);
        }

        /// <summary>
        /// Uses an already-opened serial port, and optionally takes ownership of it.
        /// </summary>
        /// <param name="port">The port to use.</param>
        /// <param name="takeOwnership">Whether to take ownership of the port. If true, calls to <see cref="Close"/> will close the port.</param>
        [ComVisible(false)]
        public void Open(SerialPort port, bool takeOwnership)
        {
            if (port == null) { throw new ArgumentNullException("port"); }
            Open(new SerialStream(port), takeOwnership);
        }

        /// <summary>
        /// Uses an already-open stream.
        /// </summary>
        /// <param name="stream">The stream to use.</param>
        [ComVisible(false)]
        public void Open(Stream stream)
        {
            Open(stream, false);
        }

        /// <summary>
        /// Uses an already-open stream, and optionally takes ownership of it.
        /// </summary>
        /// <param name="stream">The stream to use.</param>
        /// <param name="takeOwnership">Whether to take ownership of the port. If true, calls to <see cref="Close"/> will close the stream.</param>
        [ComVisible(false)]
        public void Open(Stream stream, bool takeOwnership)
        {
            Close();

            _port = stream;
            _portOwned = takeOwnership;
            _open = true;
            OnOpened();
        }

        protected virtual void OnOpened()
        {

        }

        protected virtual void OnClosed()
        {

        }

        /// <summary>
        /// Gets whether the serial port is open.
        /// </summary>
        public bool IsOpen
        {
            get { return _open; }
        }

        /// <summary>
        /// Gets the serial port being used.
        /// </summary>
        public Stream Port
        {
            get { return _port; }
        }
    }
}
