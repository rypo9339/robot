﻿#region License
/*
SyRen/Sabertooth Library for C# and .NET
Copyright (c) 2013 Dimension Engineering LLC
http://www.dimensionengineering.com/dotnet

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE
USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#endregion

using System;
using System.Runtime.InteropServices;

namespace DimensionEngineering
{
    /// <summary>
    /// Create a UsbSabertoothSerial for the serial port you are using.
    /// Then, attach a <see cref="UsbSabertooth"/> for each motor driver.
    /// </summary>
#if !NETMF
    [ClassInterface(ClassInterfaceType.AutoDispatch)]
    [ComVisible(true)]
    [Guid("4D420512-61CD-4879-B427-9269E7F9EC4A")]
    [ProgId("DE.UsbSabertoothSerial")]
#endif
    public class UsbSabertoothSerial : SerialMotorDriver
    {
        byte[] _buffer;
        int _bufferOffset;
        int _bufferLength;
        IAsyncResult _read;
        internal UsbSabertoothReplyReceiver _receiver;

        /// <summary>
        /// Initializes a new instance of the <see cref="UsbSabertoothSerial"/> class.
        /// Be sure to call <see cref="SerialMotorDriver.Open(string, int)"/>.
        /// </summary>
        public UsbSabertoothSerial()
        {
            _receiver = new UsbSabertoothReplyReceiver();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UsbSabertoothSerial"/> class.
        /// The specified serial port is opened.
        /// </summary>
        /// <param name="portName">The port to open.</param>
        /// <param name="baudRate">The baud rate to use. The factory default is 9600.</param>
        public UsbSabertoothSerial(string portName, int baudRate)
            : this()
        {
            Open(portName, baudRate);
        }

        protected override void OnOpened()
        {
            base.OnOpened();

            _buffer = new byte[256];
            _bufferOffset = 0;
            _bufferLength = 0;
            _read = null;
        }

        protected override void OnClosed()
        {
            base.OnClosed();

            if (_read != null)
            {
                try
                {
                    Port.EndRead(_read);
                }
                catch (Exception)
                {

                }
            }

            _read = null;
        }

        internal bool TryReceivePacket(UsbSabertoothTimeout timeout)
        {
            while (true)
            {
                if (!IsOpen) { return false; }

                byte @byte;
                if (!TryReceiveByte(timeout, out @byte)) { return false; }
                
                _receiver.Read(@byte);
                if (_receiver.IsReady) { return true; }
            }
        }

        bool TryReceiveByte(UsbSabertoothTimeout timeout, out byte @byte)
        {
            @byte = 0;

            if (_bufferOffset < _bufferLength)
            {
                @byte = _buffer[_bufferOffset++];
                return true;
            }
            else
            {
                if (_read == null)
                {
                    try
                    {
                        _read = Port.BeginRead(_buffer, 0, _buffer.Length, null, null);
                    }
                    catch (Exception)
                    {
                        Close(); return false;
                    }
                }
                
                if (_read.AsyncWaitHandle.WaitOne(timeout.TimeLeft))
                {
                    int bytesRead;
                    try
                    {
                        bytesRead = Port.EndRead(_read);
                    }
                    catch (TimeoutException)
                    {
                        bytesRead = 0;
                    }
                    catch (Exception)
                    {
                        _read = null;
                        Close(); return false;
                    }

                    _bufferOffset = 0;
                    _bufferLength = bytesRead;
                    _read = null;
                    if (bytesRead <= 0) { return false; }

                    @byte = _buffer[_bufferOffset++]; 
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
