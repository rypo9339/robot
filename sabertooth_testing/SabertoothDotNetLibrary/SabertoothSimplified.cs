#region License
/*
SyRen/Sabertooth Library for C# and .NET
Copyright (c) 2012-2013 Dimension Engineering LLC
http://www.dimensionengineering.com/dotnet

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE
USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#endregion

using System;
using System.Runtime.InteropServices;

namespace DimensionEngineering
{
    /// <summary>
    /// Controls a Sabertooth motor driver running in Simplified Serial mode.
    /// </summary>
#if !NETMF
    [ClassInterface(ClassInterfaceType.AutoDispatch)]
    [ComVisible(true)]
    [Guid("A8BF35B3-5F31-46AF-830B-5A337A8CED1B")]
    [ProgId("DE.SabertoothSimplified")]
#endif
    public class SabertoothSimplified : SerialMotorDriver
    {
        bool _mixed;
        int  _mixedDrive, _mixedTurn;
        bool _mixedDriveSet, _mixedTurnSet;

        /// <summary>
        /// Initializes a new instance of the <see cref="SabertoothSimplified"/> class.
        /// Be sure to call <see cref="SerialMotorDriver.Open(string, int)"/>.
        /// </summary>
        public SabertoothSimplified()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SabertoothSimplified"/> class.
        /// The specified serial port is opened.
        /// </summary>
        /// <param name="portName">The port to open.</param>
        /// <param name="baudRate">The baud rate to use.</param>
        public SabertoothSimplified(string portName, int baudRate)
            : this()
        {
            Open(portName, baudRate);
        }

        /// <summary>
        /// Sets the power of the specified motor.
        /// </summary>
        /// <param name="motorNumber">The motor number, 1 or 2.</param>
        /// <param name="power">The power, between -127 and 127.</param>
        public void Motor(int motorNumber, int power)
        {
            if (motorNumber < 1) { throw new ArgumentOutOfRangeException("motorNumber"); }

            MixedMode(false);
            if (motorNumber > 2) { return; }
            Raw(motorNumber, power);
        }

        /// <summary>
        /// Sets the power of motor 1.
        /// </summary>
        /// <param name="power">The power, between -127 and 127.</param>
        [ComVisible(false)]
        public void Motor(int power)
        {
            Motor(1, power);
        }

        /// <summary>
        /// Sets the driving power.
        /// </summary>
        /// <param name="power">The power, between -127 and 127.</param>
        public void Drive(int power)
        {
            MixedMode(true);
            _mixedDrive = Math.Max(-127, Math.Min(127, power));
            _mixedDriveSet = true;
            MixedUpdate();
        }

        /// <summary>
        /// Sets the turning power.
        /// </summary>
        /// <param name="power">The power, between -127 and 127.</param>
        public void Turn(int power)
        {
            MixedMode(true);
            _mixedTurn = Math.Max(-127, Math.Min(127, power));
            _mixedTurnSet = true;
            MixedUpdate();
        }

        /// <summary>
        /// Stops.
        /// </summary>
        public void Stop()
        {
            RequirePort();

            Port.Write(new[] { (byte)0 }, 0, 1);
            _mixedDriveSet = false;
            _mixedTurnSet = false;
        }

        void MixedMode(bool enable)
        {
            if (_mixed == enable) { return; }
  
            Stop();
            _mixed = enable;
        }

        void MixedUpdate()
        {
            if (!_mixedDriveSet || !_mixedTurnSet) { return; }
            Raw(1, _mixedDrive - _mixedTurn);
            Raw(2, _mixedDrive + _mixedTurn);
        }

        void Raw(int motor, int power)
        {
            RequirePort();

            int command, magnitude;
            power = Math.Max(-127, Math.Min(127, power));
            magnitude = Math.Abs(power) >> 1;
  
            if (motor == 1)
            {
                command = power < 0 ? 63 - magnitude : 64 + magnitude;
            }
            else
            {
                command = power < 0 ? 191 - magnitude : 192 + magnitude;
            }

            command = Math.Max(1, Math.Min(254, command));
            Port.Write(new[] { (byte)command }, 0, 1);
        }
    }
}
