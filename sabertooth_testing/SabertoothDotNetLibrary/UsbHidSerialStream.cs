﻿#region License
/*
SyRen/Sabertooth Library for C# and .NET
Copyright (c) 2014 Dimension Engineering LLC
http://www.dimensionengineering.com/dotnet

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE
USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#endregion

using System;
using System.IO;

namespace DimensionEngineering
{
    sealed class UsbHidSerialStream : Stream
    {
        byte[] _readBuffer;
        int _readBufferCount;
        int _readBufferPosition;

        public UsbHidSerialStream()
        {
            _readBuffer = new byte[60];
        }

        public UsbHidSerialStream(Stream rawStream)
            : this()
        {
            RawStream = rawStream;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposing)
            {
                if (RawStream != null) { RawStream.Dispose(); }
            }
        }

        public override void Flush()
        {
            if (RawStream == null) { throw new InvalidOperationException("RawStream is not set."); }

            RawStream.Flush();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            if (RawStream == null) { throw new InvalidOperationException("RawStream is not set."); }
            if (!CanRead) { throw new NotSupportedException(); }

            if (buffer == null) { throw new ArgumentNullException("buffer"); }
            if (offset < 0 || offset > buffer.Length) { throw new ArgumentOutOfRangeException("offset"); }
            if (count < 0 || count > buffer.Length - offset) { throw new ArgumentOutOfRangeException("count"); }

            if (_readBufferCount == _readBufferPosition)
            {
                while (true)
                {
                    byte[] hidPacket = new byte[63];
                    int hidPacketLength = RawStream.Read(hidPacket, 0, hidPacket.Length);
                    if (hidPacketLength == 0) { return 0; }

                    if (hidPacketLength == 63 && hidPacket[0] == 1 && hidPacket[1] == 0xde)
                    {
                        int hidDataLength = hidPacket[2];
                        if (hidDataLength > 0 && hidDataLength <= 60)
                        {
                            _readBufferCount = hidDataLength;
                            _readBufferPosition = 0;
                            Array.Copy(hidPacket, 3, _readBuffer, 0, hidDataLength);
                            break;
                        }
                    }
                }
            }

            int bytesRead = Math.Min(count, _readBufferCount - _readBufferPosition);
            Array.Copy(_readBuffer, _readBufferPosition, buffer, offset, bytesRead);
            _readBufferPosition += bytesRead; return bytesRead;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotSupportedException();
        }

        public override void SetLength(long value)
        {
            throw new NotSupportedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            if (RawStream == null) { throw new InvalidOperationException("RawStream is not set."); }
            if (!CanWrite) { throw new NotSupportedException(); }

            if (buffer == null) { throw new ArgumentNullException("buffer"); }
            if (offset < 0 || offset > buffer.Length) { throw new ArgumentOutOfRangeException("offset"); }
            if (count < 0 || count > buffer.Length - offset) { throw new ArgumentOutOfRangeException("count"); }

            for (int i = 0; i < count; )
            {
                byte[] hidPacket = new byte[63];
                int hidDataLength = Math.Min(31, count - i);

                hidPacket[0] = 1;
                hidPacket[1] = 0xde;
                hidPacket[2] = (byte)hidDataLength;
                Array.Copy(buffer, offset + i, hidPacket, 3, hidDataLength);

                RawStream.Write(hidPacket, 0, hidPacket.Length);
                i += hidDataLength;
            }
        }

        public override bool CanRead
        {
            get { return RawStream != null && RawStream.CanRead; }
        }

        public override bool CanSeek
        {
            get { return false; }
        }

        public override bool CanTimeout
        {
            get { return RawStream != null && RawStream.CanTimeout; }
        }

        public override bool CanWrite
        {
            get { return RawStream != null && RawStream.CanWrite; }
        }

        public override long Length
        {
            get { throw new NotSupportedException(); }
        }

        public override long Position
        {
            get { throw new NotSupportedException(); }
            set { throw new NotSupportedException(); }
        }

        public Stream RawStream
        {
            get;
            set;
        }

        public override int ReadTimeout
        {
            get
            {
                if (!CanTimeout) { throw new NotSupportedException(); }

                return RawStream.ReadTimeout;
            }

            set
            {
                if (!CanTimeout) { throw new NotSupportedException(); }

                RawStream.ReadTimeout = value;
            }
        }

        public override int WriteTimeout
        {
            get
            {
                if (!CanTimeout) { throw new NotSupportedException(); }

                return RawStream.WriteTimeout;
            }

            set
            {
                if (!CanTimeout) { throw new NotSupportedException(); }

                RawStream.WriteTimeout = value;
            }
        }
    }
}
