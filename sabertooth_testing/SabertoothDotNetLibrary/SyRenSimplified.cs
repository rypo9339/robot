#region License
/*
SyRen/Sabertooth Library for C# and .NET
Copyright (c) 2012-2013 Dimension Engineering LLC
http://www.dimensionengineering.com/dotnet

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE
USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#endregion

using System;
using System.Runtime.InteropServices;

namespace DimensionEngineering
{
    /// <summary>
    /// Controls a SyRen motor driver running in Simplified Serial mode.
    /// </summary>
#if !NETMF
    [ClassInterface(ClassInterfaceType.AutoDispatch)]
    [ComVisible(true)]
    [Guid("D0381F79-D3F4-4346-9FCD-29E824510C42")]
    [ProgId("DE.SyRenSimplified")]
#endif
    public class SyRenSimplified : SerialMotorDriver
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SyRenSimplified"/> class.
        /// Be sure to call <see cref="SerialMotorDriver.Open(string, int)"/>.
        /// </summary>
        public SyRenSimplified()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SyRenSimplified"/> class.
        /// The specified serial port is opened.
        /// </summary>
        /// <param name="portName">The port to open.</param>
        /// <param name="baudRate">The baud rate to use.</param>
        public SyRenSimplified(string portName, int baudRate)
            : this()
        {
            Open(portName, baudRate);
        }

        /// <summary>
        /// Sets the power of the specified motor.
        /// </summary>
        /// <param name="motorNumber">The motor number, 1.</param>
        /// <param name="power">The power, between -127 and 127.</param>
        public void Motor(int motorNumber, int power)
        {
            if (motorNumber < 1) { throw new ArgumentOutOfRangeException("motorNumber"); }
            if (motorNumber > 1) { return; }

            Motor(power);
        }

        /// <summary>
        /// Sets the power of the motor.
        /// </summary>
        /// <param name="power">The power, between -127 and 127.</param>
        [ComVisible(false)]
        public void Motor(int power)
        {
            RequirePort();

            byte command;
            power = Math.Max(-126, Math.Min(126, power));
            command = (byte)((power < 0 ? 127 : 128) + power);
            Port.Write(new[] { command }, 0, 1);
        }

        /// <summary>
        /// Stops.
        /// </summary>
        public void Stop()
        {
            Motor(0);
        }
    }
}
