﻿#region License
/*
SyRen/Sabertooth Library for C# and .NET
Copyright (c) 2013 Dimension Engineering LLC
http://www.dimensionengineering.com/dotnet

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE
USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#endregion

using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;

namespace DimensionEngineering
{
    /// <summary>
    /// Controls a USB-enabled Sabertooth motor driver running in Packet Serial mode.
    /// </summary>
#if !NETMF
    [ClassInterface(ClassInterfaceType.AutoDispatch)]
    [ComVisible(true)]
    [Guid("27981E36-ADED-4D7B-8DD4-518FFEDE7E43")]
    [ProgId("DE.UsbSabertooth")]
#endif
    public class UsbSabertooth
    {
        /// <summary>
        /// Get requests return this value if they time out.
        /// </summary>
        public const int GetTimedOut = int.MinValue;

        byte _address;
        UsbSabertoothSerial _connection;
        int _getRetryInterval;
        int _getTimeout;

        /// <summary>
        /// Initializes a new instance of the <see cref="UsbSabertooth"/> class.
        /// The driver address is set to 128. Be sure to set <see cref="UsbSabertooth.Connection"/>.
        /// </summary>
        public UsbSabertooth()
        {
            Address = 128;
            GetRetryInterval = UsbSabertoothConstants.DefaultGetRetryInterval;
            GetTimeout = UsbSabertoothConstants.DefaultGetTimeout;
            UsingCrc = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UsbSabertooth"/> class.
        /// The driver address is set to 128. The specified serial connection is used.
        /// </summary>
        /// <param name="connection">The serial connection to use.</param>
        public UsbSabertooth(UsbSabertoothSerial connection)
            : this()
        {
            Connection = connection;
        }

        /// <summary>
        /// Sends a multibyte packet serial command to the motor driver.
        /// </summary>
        /// <param name="command">The number of the command.</param>
        /// <param name="value">The command's value.</param>
        public void Command(byte command, byte[] value)
        {
            if (value == null) { throw new ArgumentNullException("value"); }
            if (value.Length < 1) { throw new ArgumentException("Must contain at least one byte.", "value"); }

            if (Connection == null)
            {
                throw new InvalidOperationException("Connection is not set.");
            }

            UsbSabertoothCommandWriter.WriteToStream(Connection.Port, Address,
                                                     (UsbSabertoothCommand)command, UsingCrc,
                                                     value);
        }

        /// <summary>
        /// Sends a packet serial command to the motor driver.
        /// </summary>
        /// <param name="command">The number of the command.</param>
        /// <param name="value">The command's value.</param>
        [ComVisible(false)]
        public void Command(byte command, byte value)
        {
            Command(command, new byte[1] { value });
        }

        /// <summary>
        /// Controls the specified motor output.
        /// In User Mode, this sets M1 or M2.
        /// </summary>
        /// <param name="motorOutputNumber">
        /// The motor output number, 1 or 2.
        /// You can also use a character, such as '3', to select the
        /// motor output by its Plain Text Serial address.
        /// </param>
        /// <param name="value">The value, between -2047 and 2047.</param>
        public void Motor(int motorOutputNumber, int value)
        {
            Set('M', motorOutputNumber, value);
        }

        /// <summary>
        /// Controls motor output 1.
        /// In User Mode, this sets M1.
        /// </summary>
        /// <param name="value">The value, between -2047 and 2047.</param>
        [ComVisible(false)]
        public void Motor(int value)
        {
            Motor(1, value);
        }

        /// <summary>
        /// Controls the specified power output, if the power output is configured as a controllable output.
        /// In User Mode, this sets P1 or P2.
        /// </summary>
        /// <param name="powerOutputNumber">
        /// The power output number, 1 or 2.
        /// You can also use a character, such as '3', to select the
        /// power output by its Plain Text Serial address.
        /// </param>
        /// <param name="value">The value, between -2047 and 2047.</param>
        public void Power(int powerOutputNumber, int value)
        {
            Set('P', powerOutputNumber, value);
        }

        /// <summary>
        /// Controls power output 1, if power output 1 is configured as a controllable output.
        /// In User Mode, this sets P1.
        /// </summary>
        /// <param name="value">The value, between -2047 and 2047.</param>
        [ComVisible(false)]
        public void Power(int value)
        {
            Power(1, value);
        }

        /// <summary>
        /// Controls the mixed-mode drive channel.
        /// In User Mode, this sets MD.
        /// </summary>
        /// <param name="value">The value, between -2047 and 2047.</param>
        public void Drive(int value)
        {
            Motor((int)'D', value);   
        }

        /// <summary>
        /// Controls the mixed-mode turn channel.
        /// In User Mode, this sets MT.
        /// </summary>
        /// <param name="value">The value, between -2047 and 2047.</param>
        public void Turn(int value)
        {
            Motor((int)'T', value);
        }

        /// <summary>
        /// Causes the specified motor output to freewheel.
        /// In User Mode, this sets Q1 or Q2.
        /// </summary>
        /// <param name="motorOutputNumber">
        /// The motor output number, 1 or 2.
        /// You can also use a character, such as '3', to select the
        /// motor output by its Plain Text Serial address.
        /// </param>
        /// <param name="value">
        /// A positive value lets the motor output freewheel.
        /// A negative or zero value stops the freewheeling.
        /// </param>
        public void Freewheel(int motorOutputNumber, int value)
        {
            Set('Q', motorOutputNumber, value);
        }

        /// <summary>
        /// Causes the specified motor output to freewheel.
        /// In User Mode, this sets Q1 or Q2.
        /// </summary>
        /// <param name="motorOutputNumber">
        /// The motor output number, 1 or 2.
        /// You can also use a character, such as '3', to select the
        /// motor output by its Plain Text Serial address.
        /// </param>
        /// <param name="value">
        /// <c>true</c> lets the motor output freewheel.
        /// <c>false</c> stops the freewheeling.
        /// </param>
        [ComVisible(false)]
        public void Freewheel(int motorOutputNumber, bool value)
        {
            Freewheel(motorOutputNumber, value ? 2048 : 0);
        }

        /// <summary>
        /// Causes motor output 1 to freewheel.
        /// In User Mode, this sets Q1.
        /// </summary>
        /// <param name="value">
        /// A positive value lets the motor outputs freewheel.
        /// A negative or zero value stops the freewheeling.
        /// </param>
        [ComVisible(false)]
        public void Freewheel(int value)
        {
            Freewheel(1, value);
        }

        /// <summary>
        /// Causes motor output 1 to freewheel.
        /// In User Mode, this sets Q1.
        /// </summary>
        /// <param name="value">
        /// <c>true</c> lets the motor outputs freewheel.
        /// <c>false</c> stops the freewheeling.
        /// </param>
        [ComVisible(false)]
        public void Freewheel(bool value)
        {
            Freewheel(1, value);
        }

        /// <summary>
        /// Shuts down an output.
        /// </summary>
        /// <param name="type">
        /// The type of output to shut down. This can be
        /// 'M' (motor output) or
        /// 'P' (power output).
        /// </param>
        /// <param name="number">
        /// The number of the output, 1 or 2.
        /// You can also use a character, such as '3', to select by
        /// Plain Text Serial address.
        /// </param>
        /// <param name="value">
        /// <c>true</c> sets the shutdown.
        /// <c>false</c> clears the shutdown.
        /// </param>
        public void ShutDown(char type, int number, bool value)
        {
            Set(type, number, value ? 2048 : 0, UsbSabertoothSetType.Shutdown);
        }

        void Set(char type, int number, int value, UsbSabertoothSetType setType)
        {
            byte flags = (byte)setType;
            if (value < -UsbSabertoothConstants.MaxValue) { value = -UsbSabertoothConstants.MaxValue; }
            if (value >  UsbSabertoothConstants.MaxValue) { value =  UsbSabertoothConstants.MaxValue; }
            if (value <                                0) { value = -value;              flags |=  1; }

            byte[] data = new byte[5];
            data[0] = flags;
            data[1] = (byte)((value >> 0) & 0x7f);
            data[2] = (byte)((value >> 7) & 0x7f);
            data[3] = (byte)type;
            data[4] = (byte)number;

            Command((byte)UsbSabertoothCommand.Set, data);
        }

        /// <summary>
        /// Sets a value on the motor driver.
        /// </summary>
        /// <param name="type">
        /// The type of channel to set. This can be
        /// 'M' (motor output),
        /// 'P' (power output),
        /// 'Q' (freewheel),
        /// 'R' (ramping), or
        /// 'T' (current limit).
        /// </param>
        /// <param name="number">
        /// The number of the channel, 1 or 2.
        /// You can also use a character, such as '3', to select by
        /// Plain Text Serial address.
        /// </param>
        /// <param name="value">
        /// The value, between -16383 and 16383
        /// (though in many cases, only -2047 to 2047 are meaningful).
        /// </param>
        public void Set(char type, int number, int value)
        {
            Set(type, number, value, UsbSabertoothSetType.Value);
        }

        /// <summary>
        /// Sets the current limit for the specified motor output.
        /// In User Mode, this sets T1 or T2.
        /// 2014-06-09 and newer firmwares support changing the current limit while running.
        /// </summary>
        /// <param name="motorOutputNumber">
        /// The motor output number, 1 or 2.
        /// You can also use a character, such as '3', to select the
        /// motor output by its Plain Text Serial address.
        /// </param>
        /// <param name="value">
        /// The current limit.
        /// 2048 corresponds to 100 amps. For example, 655 is 32 amps.
        /// Any negative number will use the default limit.
        /// </param>
        public void SetCurrentLimit(int motorOutputNumber, int value)
        {
            Set('T', motorOutputNumber, value);
        }

        /// <summary>
        /// Sets the current limit for all motor outputs.
        /// In User Mode, this sets T1 and T2.
        /// 2014-06-09 and newer firmwares support changing the current limit while running.
        /// </summary>
        /// <param name="value">
        /// The current limit.
        /// 2048 corresponds to 100 amps. For example, 655 is 32 amps.
        /// Any negative number will use the default limit.
        /// </param>
        [ComVisible(false)]
        public void SetCurrentLimit(int value)
        {
            SetCurrentLimit((int)'*', value);
        }

        /// <summary>
        /// Sets the ramping for the specified motor output.
        /// In User Mode, this sets R1 or R2.
        /// </summary>
        /// <param name="motorOutputNumber">
        /// The motor output number, 1 or 2.
        /// You can also use a character, such as '3', to select the
        /// motor output by its Plain Text Serial address.
        /// </param>
        /// <param name="value">The ramping value, between -16383 (fast) and 2047 (slow).</param>
        public void SetRamping(int motorOutputNumber, int value)
        {
            Set('R', motorOutputNumber, value);
        }

        /// <summary>
        /// Sets the ramping for all motor outputs.
        /// In User Mode, this sets R1 and R2.
        /// </summary>
        /// <param name="value">The ramping value, between -16383 (fast) and 2047 (slow).</param>
        [ComVisible(false)]
        public void SetRamping(int value)
        {
            SetRamping((int)'*', value);
        }

        /// <summary>
        /// Sets the serial timeout.
        /// </summary>
        /// <param name="milliseconds">
        /// The maximum time in milliseconds between packets.
        /// If this time is exceeded, the driver will stop the
        /// motor and power outputs.
        /// A value of zero uses the DEScribe setting. <see cref="Timeout.Infinite"/> disables the timeout.
        /// </param>
        public void SetTimeout(int milliseconds)
        {
            if (milliseconds < 0)
            {
                if (milliseconds != Timeout.Infinite)
                {
                    throw new ArgumentOutOfRangeException("milliseconds",
                                                          "Timeout is negative and not equal to System.Threading.Timeout.Infinite.");
                }
            }

            Set('M', (int)'*', milliseconds, UsbSabertoothSetType.Timeout);
        }

        /// <summary>
        /// Resets the serial timeout.
        /// This is done automatically any time a motor output is set.
        /// You can, however, call this if you don't want to set any motor outputs.
        /// </summary>
        public void KeepAlive()
        {
            Set('M', '*', 0, UsbSabertoothSetType.KeepAlive);
        }

        /// <summary>
        /// Gets a value from the motor driver.
        /// </summary>
        /// <param name="type">
        /// The type of channel to get from. This can be
        /// 'S' (signal),
        /// 'A' (aux),
        /// 'M' (motor output), or
        /// 'P' (power output).
        /// </param>
        /// <param name="number">
        /// The number of the channel, 1 or 2.
        /// You can also use a character, such as '3', to select by
        /// Plain Text Serial address.
        /// </param>
        /// <returns>The value, or <see cref="UsbSabertooth.GetTimedOut"/>.</returns>
        public int Get(char type, int number)
        {
            return GetSpecial(type, number, (byte)UsbSabertoothGetType.Value, false);
        }

        /// <summary>
        /// Gets the battery voltage.
        /// </summary>
        /// <param name="motorOutputNumber">
        /// The number of the motor output, 1 or 2.
        /// You can also use a character, such as '3', to select by
        /// Plain Text Serial address.
        /// </param>
        /// <param name="unscaled">If true, gets in unscaled units. If false, gets in scaled units.</param>
        /// <returns>The value, or <see cref="UsbSabertooth.GetTimedOut"/>.</returns>
        public int GetBattery(int motorOutputNumber, bool unscaled)
        {
            return GetSpecial('M', motorOutputNumber, (byte)UsbSabertoothGetType.Battery, unscaled);
        }

        /// <summary>
        /// Gets the battery voltage.
        /// </summary>
        /// <param name="motorOutputNumber">
        /// The number of the motor output, 1 or 2.
        /// You can also use a character, such as '3', to select by
        /// Plain Text Serial address.
        /// </param>
        /// <returns>The value, or <see cref="UsbSabertooth.GetTimedOut"/>.</returns>
        [ComVisible(false)]
        public int GetBattery(int motorOutputNumber)
        {
            return GetBattery(motorOutputNumber, false);
        }

        /// <summary>
        /// Gets the motor output current.
        /// </summary>
        /// <param name="motorOutputNumber">
        /// The number of the motor output, 1 or 2.
        /// You can also use a character, such as '3', to select by
        /// Plain Text Serial address.
        /// </param>
        /// <param name="unscaled">If true, gets in unscaled units. If false, gets in scaled units.</param>
        /// <returns>The value, or <see cref="UsbSabertooth.GetTimedOut"/>.</returns>
        public int GetCurrent(int motorOutputNumber, bool unscaled)
        {
            return GetSpecial('M', motorOutputNumber, (byte)UsbSabertoothGetType.Current, unscaled);
        }

        /// <summary>
        /// Gets the motor output current.
        /// </summary>
        /// <param name="motorOutputNumber">
        /// The number of the motor output, 1 or 2.
        /// You can also use a character, such as '3', to select by
        /// Plain Text Serial address.
        /// </param>
        /// <returns>The value, or <see cref="UsbSabertooth.GetTimedOut"/>.</returns>
        [ComVisible(false)]
        public int GetCurrent(int motorOutputNumber)
        {
            return GetCurrent(motorOutputNumber, false);
        }

        /// <summary>
        /// Gets the motor output temperature.
        /// </summary>
        /// <param name="motorOutputNumber">
        /// The number of the motor output, 1 or 2.
        /// You can also use a character, such as '3', to select by
        /// Plain Text Serial address.
        /// </param>
        /// <param name="unscaled">If true, gets in unscaled units. If false, gets in scaled units.</param>
        /// <returns>The value, or <see cref="UsbSabertooth.GetTimedOut"/>.</returns>
        public int GetTemperature(int motorOutputNumber, bool unscaled)
        {
            return GetSpecial('M', motorOutputNumber, (byte)UsbSabertoothGetType.Temperature, unscaled);
        }

        /// <summary>
        /// Gets the motor output temperature.
        /// </summary>
        /// <param name="motorOutputNumber">
        /// The number of the motor output, 1 or 2.
        /// You can also use a character, such as '3', to select by
        /// Plain Text Serial address.
        /// </param>
        /// <returns>The value, or <see cref="UsbSabertooth.GetTimedOut"/>.</returns>
        [ComVisible(false)]
        public int GetTemperature(int motorOutputNumber)
        {
            return GetTemperature(motorOutputNumber, false);
        }

        /// <summary>
        /// Gets a special value from the motor driver.
        /// </summary>
        /// <param name="type">The type of channel to get from.</param>
        /// <param name="number">The number of the channel.</param>
        /// <param name="getType">The get type.</param>
        /// <returns>The value, or <see cref="UsbSabertooth.GetTimedOut"/>.</returns>
        public int GetSpecial(char type, int number, int getType)
        {
            if (Connection == null)
            {
                throw new InvalidOperationException("Connection is not set.");
            }

            UsbSabertoothTimeout timeout = new UsbSabertoothTimeout(GetTimeout);
            UsbSabertoothTimeout retry = new UsbSabertoothTimeout(GetRetryInterval); retry.Expire();

            byte[] data;
            UsbSabertoothReplyCode replyCode = UsbSabertoothReplyCode.Get;

            while (true)
            {
                if (timeout.Expired)
                {
                    return GetTimedOut;
                }

                if (retry.Expired)
                {
                    retry.Reset();

                    data = new byte[3];
                    data[0] = (byte)getType;
                    data[1] = (byte)type;
                    data[2] = (byte)number;

                    Command((byte)UsbSabertoothCommand.Get, data);
                }

                if (!Connection.TryReceivePacket(timeout < retry ? timeout : retry)) { continue; }
                if (Connection._receiver.Address != Address) { continue; }
                if (Connection._receiver.Command != replyCode) { continue; }
                if (Connection._receiver.UsingCrc != UsingCrc) { continue; }

                data = Connection._receiver.Buffer;
                if (getType == (data[2] & ~1) &&
                    type == data[6] &&
                    number == data[7])
                {
                    int value = (int)data[4] << 0 |
                                (int)data[5] << 7;
                    return (data[2] & 1) != 0 ? -value : value;
                }
            }
        }

        int GetSpecial(char type, int number, byte getType, bool unscaled)
        {
            return GetSpecial(type, number, getType | (unscaled ? 2 : 0));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UsbSabertooth"/> class
        /// that communicates over the USB HID protocol.
        /// </summary>
        /// <param name="hidStream">A raw USB HID stream from the HidSharp library.</param>
        /// <returns>The new <see cref="UsbSabertooth"/> instance.</returns>
        public static UsbSabertooth FromHidStream(Stream hidStream)
        {
            if (hidStream == null) { throw new ArgumentNullException("hidStream"); }

            UsbHidSerialStream hidSerialStream = new UsbHidSerialStream(hidStream);

            UsbSabertoothSerial connection = new UsbSabertoothSerial();
            connection.Open(hidSerialStream);

            UsbSabertooth sabertooth = new UsbSabertooth(connection);
            sabertooth.Address = 136;

            return sabertooth;
        }

        /// <summary>
        /// Gets or sets the driver address. The default is 128.
        /// </summary>
        public int Address
        {
            get { return _address; }
            set
            {
                if (value < 128 || value > 255) { throw new ArgumentOutOfRangeException(); }
                _address = (byte)value;
            }
        }

        /// <summary>
        /// Gets or sets the serial connection being used.
        /// </summary>
        public UsbSabertoothSerial Connection
        {
            get { return _connection; }
            set { _connection = value; }
        }

        /// <summary>
        /// Gets or sets the get retry interval, in milliseconds.
        /// </summary>
        public int GetRetryInterval
        {
            get { return _getRetryInterval; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("Retry interval is negative or equal to System.Threading.Timeout.Infinite.",
                                                          (Exception)null);
                }

                _getRetryInterval = value;
            }
        }

        /// <summary>
        /// Gets or sets the get timeout, in milliseconds.
        /// </summary>
        public int GetTimeout
        {
            get { return _getTimeout; }
            set
            {
                if (value < 0)
                {
                    if (value != Timeout.Infinite)
                    {
                        throw new ArgumentOutOfRangeException("Timeout is negative and not equal to System.Threading.Timeout.Infinite.",
                                                              (Exception)null);
                    }
                }

                _getTimeout = value;
            }
        }

        /// <summary>
        /// Gets whether CRC-protected commands are used. They are, by default.
        /// </summary>
        public bool UsingCrc
        {
            get;
            set;
        }
    }
}
