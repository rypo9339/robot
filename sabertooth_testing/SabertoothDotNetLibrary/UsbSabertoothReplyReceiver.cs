﻿#region License
/*
SyRen/Sabertooth Library for C# and .NET
Copyright (c) 2012-2013 Dimension Engineering LLC
http://www.dimensionengineering.com/dotnet

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE
USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#endregion

namespace DimensionEngineering
{
    sealed class UsbSabertoothReplyReceiver
    {
        int _length; bool _ready, _usingCrc;
        byte[] _data = new byte[UsbSabertoothConstants.CommandMaxBufferLength];

        public UsbSabertoothReplyReceiver()
        {
            Reset();
        }

        public void Read(byte @byte)
        {
            if (@byte >= 128 || _ready) { Reset(); }
            if (_length < UsbSabertoothConstants.CommandMaxBufferLength) { _data[_length++] = @byte; }

            if (_length >= 9 && _data[0] >= 128)
            {
                bool usingCrc = (_data[0] & 0x70) == 0x70; int length;

                switch (_data[1])
                {
                    case (byte)UsbSabertoothReplyCode.Get:
                        length = usingCrc ? 10 : 9; break;

                    default:
                        return;
                }

                if (_length == length)
                {
                    if (usingCrc)
                    {
                        if (UsbSabertoothCrc7.Value(_data, 0, 3) == _data[3])
                        {
                            ushort crc = UsbSabertoothCrc14.Value(_data, 4, length - 6);

                            if (((crc >> 0) & 0x7f) == _data[length - 2] &&
                                ((crc >> 7) & 0x7f) == _data[length - 1])
                            {
                                _data[0] &= unchecked((byte)~0x70);
                                _ready = true; _usingCrc = true;
                            }
                        }
                    }
                    else
                    {
                        if (UsbSabertoothChecksum.Value(_data, 0, 3) == _data[3])
                        {
                            if (UsbSabertoothChecksum.Value(_data, 4, length - 5) == _data[length - 1])
                            {
                                _ready = true; _usingCrc = false;
                            }
                        }
                    }
                }
            }
        }

        public void Reset()
        {
            _length = 0; _ready = false; _usingCrc = false;
        }

        public int Address
        {
            get { return _data[0]; }
        }

        public byte[] Buffer
        {
            get { return _data; }
        }

        public UsbSabertoothReplyCode Command
        {
            get { return (UsbSabertoothReplyCode)_data[1]; }
        }

        public bool IsReady
        {
            get { return _ready; }
        }

        public bool UsingCrc
        {
            get { return _usingCrc; }
        }
    }
}
