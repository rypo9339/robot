﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("Dimension Engineering LLC")]
[assembly: AssemblyCopyright("Copyright © 2012-2013 Dimension Engineering LLC <http://www.dimensionengineering.com/dotnet>")]
[assembly: AssemblyProduct("SyRen/Sabertooth Library for C# and .NET")]
[assembly: AssemblyTitle("DimensionEngineering.Sabertooth")]
[assembly: AssemblyVersion("1.6.0.0")]
[assembly: AssemblyFileVersion("1.6.0.0")]

#if !NETMF  
[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]
[assembly: Guid("B2121582-2E62-4DC1-9D22-6528FBC57869")]
#endif