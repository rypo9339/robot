﻿#region License
/*
SyRen/Sabertooth Library for C# and .NET
Copyright (c) 2013 Dimension Engineering LLC
http://www.dimensionengineering.com/dotnet

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE
USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#endregion

namespace DimensionEngineering
{
    struct UsbSabertoothCrc14
    {
        ushort _crc;

        public void Begin()
        {
            _crc = 0x3fff;
        }

        public void Write(byte data)
        {
            _crc ^= data;

            for (int bit = 0; bit < 8; bit++)
            {
                if (0 != (_crc & 1))
                {
                    _crc >>= 1; _crc ^= 0x22f0;
                }
                else
                {
                    _crc >>= 1;
                }
            }
        }

        public void Write(byte[] data, int offset, int length)
        {
            for (int i = 0; i < length; i ++) { Write(data[offset + i]); }
        }


        public void End()
        {
            _crc ^= 0x3fff;
        }

        public ushort Value()
        {
            return _crc;
        }

        public void Value(ushort crc)
        {
            _crc = crc;
        }

        public static ushort Value(byte[] data, int offset, int length)
        {
            UsbSabertoothCrc14 crc = new UsbSabertoothCrc14();
            crc.Begin(); crc.Write(data, offset, length); crc.End();
            return crc.Value();
        }
    }
}