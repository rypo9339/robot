﻿#region License
/*
SyRen/Sabertooth Library for C# and .NET
Copyright (c) 2012-2013 Dimension Engineering LLC
http://www.dimensionengineering.com/dotnet

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE
USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#endregion

using System;
using System.IO;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Threading;

namespace DimensionEngineering
{
    /// <summary>
    /// Controls a Sabertooth or SyRen motor driver running in Packet Serial mode.
    /// </summary>
#if !NETMF
    [ClassInterface(ClassInterfaceType.AutoDispatch)]
    [ComVisible(true)]
    [Guid("3ED2FC66-C91E-47CA-B91D-EC877AC526D2")]
    [ProgId("DE.Sabertooth")]
#endif
    public class Sabertooth : SerialMotorDriver
    {
        byte _address;
        byte[] _buffer;

        /// <summary>
        /// Initializes a new instance of the <see cref="Sabertooth"/> class.
        /// The driver address is set to 128. Be sure to call <see cref="SerialMotorDriver.Open(string, int)"/>.
        /// </summary>
        public Sabertooth()
        {
            _buffer = new byte[4]; // For Netduino's sake, reduce GC pressure by preallocating the send buffer.
            Address = 128;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Sabertooth"/> class.
        /// The driver address is set to 128. The specified serial port is opened.
        /// </summary>
        /// <param name="portName">The port to open.</param>
        /// <param name="baudRate">The baud rate to use. The factory default is 9600.</param>
        public Sabertooth(string portName, int baudRate)
            : this()
        {
            Open(portName, baudRate);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Sabertooth"/> class.
        /// The driver address is set to the value given, and the specified serial port is opened.
        /// </summary>
        /// <param name="address">The driver address.</param>
        /// <param name="portName">The port to open.</param>
        /// <param name="baudRate">The baud rate to use.</param>
        public Sabertooth(int address, string portName, int baudRate)
            : this()
        {
            Address = address;
            Open(portName, baudRate);
        }

        /// <summary>
        /// Sends the autobaud character.
        /// </summary>
        /// <param name="port">The stream to write to.</param>
        /// <param name="doNotWait">If false, a delay is added to give the driver time to start up.</param>
        [ComVisible(false)]
        public static void AutoBaud(Stream port, bool doNotWait)
        {
            if (port == null) { throw new ArgumentNullException("port"); }

            if (!doNotWait) { Thread.Sleep(1500); }
            port.Write(new[] { (byte)0xaa }, 0, 1);
            port.Flush();
            if (!doNotWait) { Thread.Sleep(500); }
        }

        /// <summary>
        /// Sends the autobaud character.
        /// </summary>
        /// <param name="port">The stream to write to.</param>
        [ComVisible(false)]
        public static void AutoBaud(Stream port)
        {
            AutoBaud(port, false);
        }

        /// <summary>
        /// Sends the autobaud character.
        /// </summary>
        /// <param name="port">The serial port to send to.</param>
        /// <param name="doNotWait">If false, a delay is added to give the driver time to start up.</param>
        [ComVisible(false)]
        public static void AutoBaud(SerialPort port, bool doNotWait)
        {
            if (port == null) { throw new ArgumentNullException("port"); }
            AutoBaud(new SerialStream(port), doNotWait);
        }

        /// <summary>
        /// Sends the autobaud character.
        /// </summary>
        /// <param name="port">The serial port to send to.</param>
        [ComVisible(false)]
        public static void AutoBaud(SerialPort port)
        {
            AutoBaud(port, false);
        }

        /// <summary>
        /// Sends the autobaud character.
        /// </summary>
        /// <param name="doNotWait">If false, a delay is added to give the driver time to start up.</param>
        public void AutoBaud([Optional] bool doNotWait)
        {
            RequirePort();
            AutoBaud(Port, doNotWait);
        }

        /// <summary>
        /// Sends the autobaud character.
        /// </summary>
        [ComVisible(false)]
        public void AutoBaud()
        {
            AutoBaud(false);
        }

        /// <summary>
        /// Sends a Packet Serial command to the motor driver.
        /// </summary>
        /// <param name="command">The number of the command.</param>
        /// <param name="value">The command's value.</param>
        public void Command(int command, int value)
        {
            if (command < 0 || command > 127) { throw new ArgumentOutOfRangeException("command"); }
            if (value < 0 || value > 127) { throw new ArgumentOutOfRangeException("value"); }

            RequirePort();
            lock (_buffer)
            {
                _buffer[0] = (byte)Address;
                _buffer[1] = (byte)command;
                _buffer[2] = (byte)value;
                _buffer[3] = (byte)((_buffer[0] + _buffer[1] + _buffer[2]) & 127);
                Port.Write(_buffer, 0, _buffer.Length);
            }
        }

        void ThrottleCommand(int command, int power)
        {
            power = Math.Max(-126, Math.Min(126, power));
            Command(command, Math.Abs(power));
        }

        /// <summary>
        /// Sets the power of the specified motor.
        /// </summary>
        /// <param name="motorNumber">The motor number, 1 or 2.</param>
        /// <param name="power">The power, between -127 and 127.</param>
        public void Motor(int motorNumber, int power)
        {
            if (motorNumber < 1) { throw new ArgumentOutOfRangeException("motorNumber"); }
            if (motorNumber > 2) { return; }
            ThrottleCommand((motorNumber == 2 ? 4 : 0) + (power < 0 ? 1 : 0), power);
        }

        /// <summary>
        /// Sets the power of motor 1.
        /// </summary>
        /// <param name="power">The power, between -127 and 127.</param>
        [ComVisible(false)]
        public void Motor(int power)
        {
            Motor(1, power);
        }

        /// <summary>
        /// Sets the driving power.
        /// </summary>
        /// <param name="power">The power, between -127 and 127.</param>
        public void Drive(int power)
        {
            ThrottleCommand(power < 0 ? 9 : 8, power);
        }

        /// <summary>
        /// Sets the turning power.
        /// </summary>
        /// <param name="power">The power, between -127 and 127.</param>
        public void Turn(int power)
        {
            ThrottleCommand(power < 0 ? 11 : 10, power);
        }

        /// <summary>
        /// Stops.
        /// </summary>
        public void Stop()
        {
            Motor(1, 0);
            Motor(2, 0);
        }

        /// <summary>
        /// Sets the minimum voltage.
        /// </summary>
        /// <param name="value">
        /// The voltage. The units of this value are driver-specific and are specified
        /// in the Packet Serial chapter of the driver's user manual.
        /// </param>
        public void SetMinVoltage(int value)
        {
            if (value < 0 || value > 120) { throw new ArgumentOutOfRangeException("value"); }
            Command(2, value);
        }

        /// <summary>
        /// Sets the maximum voltage.
        /// Maximum voltage is stored in EEPROM, so changes persist between power cycles.
        /// </summary>
        /// <param name="value">
        /// The voltage. The units of this value are driver-specific and are specified
        /// in the Packet Serial chapter of the driver's user manual.
        /// </param>
        public void SetMaxVoltage(int value)
        {
            if (value < 0 || value > 127) { throw new ArgumentOutOfRangeException("value"); }
            Command(3, value);
        }

        /// <summary>
        /// Sets the baud rate.
        /// Baud rate is stored in EEPROM, so changes persist between power cycles.
        /// </summary>
        /// <param name="baudRate">The baud rate. This can be 2400, 9600, 19200, 38400, or on some drivers 115200.</param>
        public void SetBaudRate(int baudRate)
        {
            RequirePort();
            Port.Flush();

            int value;
            switch (baudRate)
            {
                case 2400: value = 1; break;
                case 9600: value = 2; break;
                case 19200: value = 3; break;
                case 38400: value = 4; break;
                case 115200: value = 5; break;
                default: throw new ArgumentException("Unsupported baud rate.", "baudRate");
            }
            Command(15, value);

            Port.Flush();
            // (1) Flush may return early.
            // (2) Sabertooth takes about 200 ms after setting the baud rate to
            //     respond to commands again (it restarts).
            // So, this 500 ms delay should deal with this.
            Thread.Sleep(500);
        }

        /// <summary>
        /// Sets the deadband.
        /// Deadband is stored in EEPROM, so changes persist between power cycles.
        /// </summary>
        /// <param name="value">
        /// The deadband value.
        /// Motor powers in the range [-deadband, deadband] will be considered in the deadband, and will
        /// not prevent the driver from entering nor cause the driver to leave an idle brake state.
        /// 0 resets to the default, which is 3.
        /// </param>
        public void SetDeadband(int value)
        {
            if (value < 0 || value > 127) { throw new ArgumentOutOfRangeException("value"); }
            Command(17, value);
        }

        /// <summary>
        /// Sets the ramping.
        /// Ramping is stored in EEPROM, so changes persist between power cycles.
        /// </summary>
        /// <param name="value">The ramping value. Consult the user manual for possible values.</param>
        public void SetRamping(int value)
        {
            if (value < 0 || value > 80) { throw new ArgumentOutOfRangeException("value"); }
            Command(18, value);
        }

        /// <summary>
        /// Sets the serial timeout.
        /// </summary>
        /// <param name="milliseconds">
        /// The maximum time in milliseconds between packets. If this time is exceeded,
        /// the driver will stop the motors. This value is rounded up to the nearest 100 milliseconds.
        /// This library assumes the command value is in units of 100 milliseconds. This is true for
        /// most drivers, but not all. Check the Packet Serial chapter of the driver's user manual
        /// to make sure.
        /// </param>
        public void SetTimeout(int milliseconds)
        {
            if (milliseconds < 0 || milliseconds > 12700) { throw new ArgumentOutOfRangeException("milliseconds"); }
            Command(14, (milliseconds + 99) / 100);
        }

        /// <summary>
        /// Gets or sets the driver address. The default is 128.
        /// </summary>
        public int Address
        {
            get { return _address; }
            set
            {
                if (value < 128 || value > 255) { throw new ArgumentOutOfRangeException(); }
                _address = (byte)value;
            }
        }
    }
}
