﻿#region License
/*
SyRen/Sabertooth Library for C# and .NET
Copyright (c) 2013 Dimension Engineering LLC
http://www.dimensionengineering.com/dotnet

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE
USE OR PERFORMANCE OF THIS SOFTWARE.
*/
#endregion

using System;
using System.Threading;

namespace DimensionEngineering
{
    /// <summary>
    /// Encapsulates a starting time and duration.
    /// </summary>
    struct UsbSabertoothTimeout
    {
        int _start; int? _timeoutMS;

        /// <summary>
        /// Constructs a UsbSabertoothTimeout object. The starting time is captured.
        /// </summary>
        /// <param name="timeoutMS">The timeout duration, in milliseconds.</param>
        public UsbSabertoothTimeout(int timeoutMS)
        {
            if (timeoutMS < 0)
            {
                if (timeoutMS != Timeout.Infinite)
                {
                    throw new ArgumentOutOfRangeException("timeoutMS",
                                                          "Timeout is negative and not equal to System.Threading.Timeout.Infinite.");
                }

                _timeoutMS = null;
            }
            else
            {
                _timeoutMS = timeoutMS;
            }

            _start = Environment.TickCount;
        }

        /// <summary>
        /// Causes the timeout to expire immediately.
        /// </summary>
        public void Expire()
        {
            if (_timeoutMS == null) { return; }
            _start = Environment.TickCount - (int)_timeoutMS;
        }

        /// <summary>
        /// Captures the current time and uses it as the new starting time for the timeout.
        /// </summary>
        public void Reset()
        {
            _start = Environment.TickCount;
        }

        /// <summary>
        /// Gets whether the timeout can expire.
        /// If the UsbSabertoothTimeout was created with a timeout of <see cref="Timeout.Infinite"/>, it cannot expire.
        /// </summary>
        public bool CanExpire
        {
            get { return _timeoutMS != null; }
        }

        /// <summary>
        /// Gets whether the timeout expired.
        /// </summary>
        public bool Expired
        {
            get { return TimeLeft == 0; }
        }

        /// <summary>
        /// Gets the time left before expiration, or <see cref="Timeout.Infinite"/> if the timeout cannot expire.
        /// </summary>
        public int TimeLeft
        {
            get
            {
                if (!CanExpire) { return Timeout.Infinite; }

                uint elapsedTime = (uint)(Environment.TickCount - _start);
                uint allowedTime = (uint)(int)_timeoutMS;
                return elapsedTime >= allowedTime ? 0 : (int)(allowedTime - elapsedTime);
            }
        }

        /// <summary>
        /// Compares the amount of time left in the timeouts.
        /// </summary>
        /// <param name="lhs">The left side of the less-than comparison.</param>
        /// <param name="rhs">The right side of the less-than comparison.</param>
        /// <returns>True if <paramref name="lhs"/> has less time left than <paramref name="rhs"/>.</returns>
        public static bool operator <(UsbSabertoothTimeout lhs, UsbSabertoothTimeout rhs)
        {
            return (uint)lhs.TimeLeft < (uint)rhs.TimeLeft;
        }

        /// <summary>
        /// Compares the amount of time left in the timeouts.
        /// </summary>
        /// <param name="lhs">The left side of the greater-than comparison.</param>
        /// <param name="rhs">The right side of the greater-than comparison.</param>
        /// <returns>True if <paramref name="lhs"/> has more time left than <paramref name="rhs"/>.</returns>
        public static bool operator >(UsbSabertoothTimeout lhs, UsbSabertoothTimeout rhs)
        {
            return (uint)lhs.TimeLeft > (uint)rhs.TimeLeft;
        }
    }
}
