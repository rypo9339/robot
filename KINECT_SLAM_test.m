% Initialization
node = ros2node("matlab_node");
disp('Node initialized');

imgSub = ros2subscriber(node, "/image_raw", "sensor_msgs/Image");
disp('Image subscriber initialized');

pcSub = ros2subscriber(node, "/points", "sensor_msgs/PointCloud2");
try
    pcMsg = receive(pcSub, 30);
    disp('PointCloud message received');
    disp(pcMsg);  % Display the received message
catch
    disp('Error receiving PointCloud message');
end

try
    infoMsg = receive(infoSub, 30);
    disp('CameraInfo message received');
    disp(infoMsg); % Display the received message
catch
    disp('Error receiving CameraInfo message');
end


infoSub = ros2subscriber(node, "/camera_info", "sensor_msgs/CameraInfo");
disp('CameraInfo subscriber initialized');


% Receive Camera Info and extract matrices
infoMsg = receive(infoSub, 30); % Adjust timeout as needed
k = reshape(infoMsg.k, [3, 3])'; % Intrinsic matrix
disp('k matrix extracted');
r = reshape(infoMsg.r, [3, 3])'; % Rectification matrix (if stereo)
disp('r matrix extracted');
p = reshape(infoMsg.p, [4, 3])'; % Projection matrix
disp('p matrix extracted');

% Create a figure for visualization
fig = figure('Name', 'Overlayed Depth on RGB');

while ishandle(fig)
    disp('Starting new loop iteration');
    % Read the messages
    imgMsg = receive(imgSub, 30); % Adjust timeout as needed
    pcMsg = receive(pcSub, 30);
    
    % Convert ROS image message to MATLAB image
    img = readImage(imgMsg);
    
    % Convert ROS PointCloud2 message to MATLAB pointCloud object
    ptCloud = pointCloud2ToPointCloud(pcMsg);
    
    % Projecting the point cloud
    [u, v] = worldToImage(k, r, p(1:3, 4), ptCloud.Location, 'ApplyDistortion', false);
    
    % Create a mask or use other blending methods
    mask = false(size(img,1), size(img,2));
    for i = 1:length(u)
        if round(v(i)) > 0 && round(v(i)) <= size(img,1) && round(u(i)) > 0 && round(u(i)) <= size(img,2)
            mask(round(v(i)), round(u(i))) = true;
        end
    end
    
    % Blend or overlay the depth onto the RGB image
    overlayedImg = imfuse(img, mask, 'blend');
    
    imshow(overlayedImg);
    drawnow; % Forces MATLAB to update the figure
    disp('Loop iteration completed');
end

% Cleanup
disp('Cleaning up...');
clear node;
node = ros2node("matlab_node");
disp('Node reinitialized');


