% Initialization
ros2node("initialize");
node = ros2node("matlab_node");
pcSub = ros2subscriber(node, "/camera/depth/points", "sensor_msgs/PointCloud2");

% Wait for a short time to allow messages to be received
pause(5);

% Try to retrieve the latest message
pcMsg = receive(pcSub, 10); % Wait for up to 10 seconds

% Display the message
disp(pcMsg);

