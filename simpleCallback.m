function varCallback(varargin)
    for i = 1:nargin
        disp(['Argument ', num2str(i), ':']);
        disp(varargin{i});
    end
end
