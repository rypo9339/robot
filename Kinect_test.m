ros2node("initialize");
ros2("node","list");
node = ros2node("matlab_node");
imgSub = ros2subscriber(node, "/camera/color/image_raw");

% Create a figure to display the image
hFig = figure('Name', 'Live Video Feed', 'NumberTitle', 'off');

% Continue updating the video feed until the figure is closed
while ishandle(hFig)
    % Receive an image message from the subscriber (with a short timeout to keep display updated)
    imgMsg = receive(imgSub, 1);

    % Determine if the image is grayscale or color based on the encoding
    if strcmp(imgMsg.encoding, 'mono8') % Grayscale
        imgData = uint8(reshape(imgMsg.data, [imgMsg.width, imgMsg.height])');
    elseif strcmp(imgMsg.encoding, 'rgb8') % Color
        imgData = uint8(reshape(imgMsg.data, [3, imgMsg.width, imgMsg.height]));
        imgData = permute(imgData, [3, 2, 1]);
    else
        error('Unsupported image encoding.');
    end

    % Update the display
    imshow(imgData);
    title('Live Video Feed');
    
    % Allow figure to update and process any callbacks
    drawnow;
end
rosPlot(scanMsg);
rosPlot(ptcloudMsg);

clear node

