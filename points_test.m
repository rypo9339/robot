% Initialization
global messages; % Create a global variable to store received messages
messages = {}; % Initialize as an empty cell array

ros2node("initialize");
node = ros2node("matlab_node");
pcSub = ros2subscriber(node, "/camera/depth/points");
disp(pcSub);
% Subscribe with callback
sub = ros2subscriber(node, '/camera/depth/points');

% Monitor for approximately 30 seconds
startTime = tic;
while toc(startTime) < 30
    % Display the length of the messages variable
    disp(['Number of messages received: ', num2str(messages)]);
    pause(0.5);  % Pause for half a second
end

% Clean up
clear node;

% Callback function
function varCallback(~, msg)
    global messages;
    messages{end+1} = msg; % Append the new message to the global variable
end
